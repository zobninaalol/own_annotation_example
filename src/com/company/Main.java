package com.company;

import java.lang.annotation.Annotation;

public class Main {

    public static void main(String[] args) {
        inspectService(SimpleService.class);
        inspectService(LazyService.class);
        inspectService(String.class);
    }

    static void inspectService(Class<?> service){
        if(service.isAnnotationPresent(Service.class)){
            Service ann = service.getAnnotation(Service.class);
            System.out.println("Has annotation Service");
            System.out.println("Name = " + ann.name());
            System.out.println("LazyLoad = " + ann.lazyLoad());
        }
    }
}
