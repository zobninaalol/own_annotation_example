package com.company;

import java.lang.annotation.*;

@Documented //попадает в javadoc
@Inherited //наследуется потомками
@Target(ElementType.TYPE) //область применения
@Retention(RetentionPolicy.RUNTIME) //время жизни
public @interface Service {
    String name();

    boolean lazyLoad() default false;
}
