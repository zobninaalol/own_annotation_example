package com.company;

@Service(name = "VerySimpleService")
public class SimpleService {
    @Init
    public void initService(){
        System.out.println("initService");
    }

    public void method(){
        System.out.println("method");
    }
}
