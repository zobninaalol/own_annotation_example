package com.company;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Init { //аннотация для методов
    boolean suppressException() default false;
}
