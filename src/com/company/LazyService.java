package com.company;

@Service(name = "VeryLazyService", lazyLoad = true)
public class LazyService {
    @Init(suppressException = true)
    public void lazyInit() throws Exception {
        System.out.println("lazyService");
    }
}
